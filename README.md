# Let's Encrypt sur Rancher

Installer<br>
https://cert-manager.io/docs/installation/kubernetes/

## Configuration et activation de l'autorité de certification de prod

```bash
cat <<EOF > prod.yaml
   apiVersion: cert-manager.io/v1alpha2
   kind: ClusterIssuer
   metadata:
     name: letsencrypt-prod
   spec:
     acme:
       # The ACME server URL
       server: https://acme-v02.api.letsencrypt.org/directory
       # Email address used for ACME registration
       email: antoinee.guerin@gmail.com
       # Name of a secret used to store the ACME account private key
       privateKeySecretRef:
         name: letsencrypt-prod
       # Enable the HTTP-01 challenge provider
       solvers:
       - http01:
           ingress:
             class:  nginx
EOF
```

```bash
kubectl create -f prod.yaml
```

## Configuration et activation de l'autorité de certification de prod

```bash
cat <<EOF > toto-tls.yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: toto
  namespace: toto
  annotations:
    kubernetes.io/ingress.class: "nginx"    
    cert-manager.io/cluster-issuer: "letsencrypt-prod"

spec:
  tls:
  - hosts:
    - toto.antoine-guerin.com
    secretName: toto-tls
  rules:
  - host: toto.antoine-guerin.com
    http:
      paths:
      - path: /
        backend:
          serviceName: toto
          servicePort: 80
EOF
```

```bash
kubectl create -f toto-tls.yaml
```

## Pour suivre l'avancé de la génération du cerficat

```bash
kubectl describe certificate toto-tls
```

## Référence installation du cert manager
[CERT-MANAGER](https://cert-manager.io/docs/installation/kubernetes/)
